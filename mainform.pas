unit MainForm;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, ExtCtrls,
  Buttons, ActnList, ComCtrls, SpinEx, Types;

type

  { TForm1 }

  TForm1 = class(TForm)
    BitBtn1: TBitBtn;
    FUsage: TComboBox;
    FrontPresText: TStaticText;
    FWidth: TFloatSpinEditEx;
    FWeigth: TFloatSpinEditEx;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    Label3: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    PageControl1: TPageControl;
    Panel1: TPanel;
    RearPresText: TStaticText;
    FDistribution: TSpinEditEx;
    StaticText1: TStaticText;
    StaticText2: TStaticText;
    StaticText3: TStaticText;
    StaticText4: TStaticText;
    StaticText5: TStaticText;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    procedure BitBtn1Click(Sender: TObject);
    procedure InputDataChange(Sender: TObject);
  private

  public

  end;

var
  Form1: TForm1;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.InputDataChange(Sender: TObject);
begin
  FPONotifyObservers(Self, ooChange, nil);
end;

procedure TForm1.BitBtn1Click(Sender: TObject);
begin
  FPONotifyObservers(Self, ooCustom, nil);
end;

end.

