unit PressureChartForm;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, TAGraph, TASeries, TADrawUtils, TACustomSeries;

type

  { TForm2 }

  TForm2 = class(TForm)
    Chart1: TChart;
    Chart1LineSeries1: TLineSeries;
    procedure FormCreate(Sender: TObject);
  private

  public

  end;

var
  Form2: TForm2;

implementation

{$R *.lfm}

type
  TPressureData = array[0..15] of array[0..1] of single;

const
  PRESSURE_DATA: TPressureData = (
    (20.0, 9.0),
    (23.0, 8.0),
    (25.0, 7.0),
    (28.0, 6.0),
    (30.0, 5.5),
    (32.0, 5.0),
    (35.0, 4.5),
    (37.0, 4.5),
    (40.0, 4.0),
    (42.0, 4.0),
    (44.0, 3.5),
    (47.0, 3.5),
    (50.0, 3.0),
    (54.0, 2.5),
    (57.0, 2.2),
    (60.0, 2.0)
    );

{ TForm2 }

procedure TForm2.FormCreate(Sender: TObject);
var
  i: Integer;
begin
  for i := Low(PRESSURE_DATA) to High(PRESSURE_DATA) do
    Chart1LineSeries1.AddXY(PRESSURE_DATA[i][0], PRESSURE_DATA[i][1]);
end;

end.

