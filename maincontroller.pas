unit MainController;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, MainForm;

type
  TController1 = class(TObject, IFPObserver)
    procedure FPOObservedChanged(ASender: TObject; Operation: TFPObservedOperation;
      Data: Pointer);
  private
    procedure ShowPressureChart;
    procedure UpdatePressures(GUI: TForm1);
  end;

implementation

uses
  PressureChartForm;

type
  TPressureData = array[0..15] of array[0..1] of single;
  TUsage = (usgCyclocross, usgCity, usgMTB, usgRoad);

const
  PRESSURE_DATA: TPressureData = (
    (20.0, 9.0),
    (23.0, 8.0),
    (25.0, 7.0),
    (28.0, 6.0),
    (30.0, 5.5),
    (32.0, 5.0),
    (35.0, 4.5),
    (37.0, 4.5),
    (40.0, 4.0),
    (42.0, 4.0),
    (44.0, 3.5),
    (47.0, 3.5),
    (50.0, 3.0),
    (54.0, 2.5),
    (57.0, 2.2),
    (60.0, 2.0)
    );

procedure BergfreundeAlgorithm(AWidth: double; AWeight: double;
  ADistrb: double; out Front: double; out Rear: double); forward;

procedure WiggleAlgorithm(AUsage: TUsage; AWeight: double; out Front: double;
  out Rear: double); forward;

{ TController1 }

procedure TController1.FPOObservedChanged(ASender: TObject;
  Operation: TFPObservedOperation; Data: Pointer);

var
  GUI: TForm1;

begin
  GUI := ASender as TForm1;

  case Operation of
    ooChange:
      UpdatePressures(GUI);
    ooCustom:
      ShowPressureChart;
    ooFree:
      Free;
    else
      raise EObserver.Create('Unexpected operation');
  end;
end;

procedure TController1.ShowPressureChart;
begin
  Form2.ShowModal;
  Form2.Hide;
end;

procedure TController1.UpdatePressures(GUI: TForm1);

type
  TUsageLUT = array[0..3] of TUsage;

const
  UsageLUT: TUsageLUT = (usgCyclocross, usgCity, usgMTB, usgRoad);

var
  Front: double;
  Rear: double;

begin
  case GUI.PageControl1.ActivePageIndex of
    0:
      BergfreundeAlgorithm(
        GUI.FWidth.Value,
        GUI.FWeigth.Value,
        GUI.FDistribution.Value,
        Front, Rear
        );

    1:
      WiggleAlgorithm(
        UsageLUT[GUI.FUsage.ItemIndex],
        GUI.FWeigth.Value,
        Front, Rear
        );

    else
      Exit;
  end;

  GUI.FrontPresText.Caption := Front.ToString(ffFixed, 1, 1);
  GUI.RearPresText.Caption := Rear.ToString(ffFixed, 1, 1);
end;

{ TController1 }

procedure BergfreundeAlgorithm(AWidth: double; AWeight: double;
  ADistrb: double; out Front: double; out Rear: double);

var
  Index: integer;
  BaseP: single;

begin
  Assert((AWidth >= 20.0) and (AWidth <= 60.0));
  Assert((AWeight >= 40.0) and (AWeight <= 120.0));
  Assert((ADistrb >= 0.0) and (ADistrb <= 100.0));

  // Tire width effect
  index := Trunc(15.0 * (AWidth - 20.0) / (60.0 - 20.0));
  while AWidth > PRESSURE_DATA[Index][0] do
    Inc(Index);
  if Index > 0 then
    repeat
      Dec(Index);
    until AWidth > PRESSURE_DATA[Index][0];
  BaseP := (PRESSURE_DATA[Index][1] + (AWidth - PRESSURE_DATA[Index][0]) *
    (PRESSURE_DATA[Index + 1][1] - PRESSURE_DATA[Index][1]) /
    (PRESSURE_DATA[Index + 1][0] - PRESSURE_DATA[Index][0]));

  // Total weight effect
  BaseP := BaseP * (1.0 + 0.01 * (AWeight - 75.0));

  // Weight distribution effect
  Front := 0.02 * ADistrb * BaseP;
  Rear := (2.0 - 0.02 * ADistrb) * BaseP;
end;

procedure WiggleAlgorithm(AUsage: TUsage; AWeight: double; out Front: double;
  out Rear: double);

const
  psi2bar = 0.0689476;

begin
  case AUsage of
    usgCyclocross:
    begin // Cyclocross
      Front := psi2bar * (48.0 + (AWeight - 70.0) / 5.0);
      Rear := psi2bar * (50.0 + (AWeight - 70.0) / 5.0);
    end;

    usgCity:
    begin // City
      Front := psi2bar * (50.0 + (AWeight - 70.0) / 5.0);
      Rear := psi2bar * (55.0 + (AWeight - 70.0) / 5.0);
    end;

    usgMTB:
    begin // MTB
      Front := psi2bar * (36.0 + (AWeight - 70.0) / 5.0);
      Rear := psi2bar * (38.0 + (AWeight - 70.0) / 5.0);
    end;

    usgRoad:
    begin // Road
      Front := psi2bar * (90.0 + 2.0 * (AWeight - 70.0) / 5.0);
      Rear := psi2bar * (93.0 + 2.0 * (AWeight - 70.0) / 5.0);
    end;
  end;
end;

end.
